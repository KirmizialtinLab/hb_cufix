# HB_CUFIX

This includes a gromacs compatible force field folder of HB-CUFIX force field published  (J. Phys. Chem. Lett. 2022, 13, 15, 3400–3408). This HB CUFIX modification extends the CUFIX corrections further to improve RNA structures. We benchmarked it with Helix-Junction-Helix duplex and with single-strand RNA systems.                                       

Refining the RNA Force Field with Small-Angle X-ray Scattering of Helix–Junction–Helix RNA  
Weiwei He, Nawavi Naleem, Diego Kleiman, and Serdal Kirmizialtin  
The Journal of Physical Chemistry Letters 2022 13 (15), 3400-3408  
DOI: 10.1021/acs.jpclett.2c00359

If you need further information then contact Kirmizialtin Lab.

https://www.kirmizialtinlab.org

The user's advised to check and validate.  

## Installation

copy the HB_cufix_RNA.ff  folder to the working directory.

```bash
cp  -r  HB_cufix_RNA.ff         \Your\work\directory
```

## Usage

Select the HB_CUFIX force field:

```bash
gmx pdb2gmx -f RNA_struc.pdb


Select the Force Field:
From current directory:
 1: Amber_HB_CUFIX for RNA : Extended CUFIX ff (J. Phys. Chem. Lett. 2022, 13, 15, 3400-3408)  
From '/share........./gromacs/top':
 2: AMBER03 protein, nucleic AMBER94 (Duan et al., J. Comp. Chem. 24, 1999-2012, 2003)
 3: AMBER94 force field (Cornell et al., JACS 117, 5179-5197, 1995)

```

